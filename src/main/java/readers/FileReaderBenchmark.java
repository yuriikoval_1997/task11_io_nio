package readers;

import java.io.*;
import java.util.Date;

public class FileReaderBenchmark {
    public static void readWithoutBuffer(File file) throws IOException {
        System.out.println("====Test reading from file without buffer ====");
        Date start = new Date();
        System.out.printf("Start. File size = %d byte.", file.length());
        System.out.println();
        try (FileInputStream fileInputStream = new FileInputStream(file)) {
            StringBuilder builder = new StringBuilder();
            while (fileInputStream.available() > 0) {
                builder.append(fileInputStream.read());
            }
            String result = builder.toString();
            Date finish = new Date();
            System.out.printf("End. It took %d ms to read whole file without buffer.\n",
                    (finish.getTime() - start.getTime()));
            System.out.println("===============================================");
        }
    }

    public static void readWithBuffer(File file) throws IOException {
        System.out.println("====Test reading from file with buffer ====");
        Date start = new Date();
        System.out.printf("Start. File size = %d byte. Buffer size 1 MB", file.length());
        System.out.println();
        try (BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file))) {
            StringBuilder builder = new StringBuilder();
            byte[] buffer = new byte[(int) Math.pow(2, 20)];
            while (bufferedInputStream.available() > 0) {
                int readBytes = bufferedInputStream.read(buffer);
                builder.append(new String(buffer, 0, readBytes));
            }
            String result = builder.toString();
            Date finish = new Date();
            System.out.printf("End. It took %d ms to read whole file with 1 MB buffer.\n",
                    (finish.getTime() - start.getTime()));
            System.out.println("===============================================");
        }
    }

    public static void readWithBufferedReader(File file) throws IOException {
        System.out.println("====Test reading from file with BufferedReader ====");
        Date start = new Date();
        System.out.printf("Start. File size = %d byte. Buffer size 1 MB", file.length());
        System.out.println();
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
            StringBuilder builder = new StringBuilder();
            char[] buffer = new char[(int) (Math.pow(2, 19))];
            while (bufferedReader.ready()){
                int readChars = bufferedReader.read(buffer);
                builder.append(buffer, 0, readChars);
            }
            String result = builder.toString();
            Date finish = new Date();
            System.out.printf("End. It took %d ms to read whole file with BufferedReader, 1 MB buffer.\n",
                    (finish.getTime() - start.getTime()));
            System.out.println("===============================================");
        }
    }
}
