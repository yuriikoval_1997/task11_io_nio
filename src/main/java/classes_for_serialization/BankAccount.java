package classes_for_serialization;

import java.io.Serializable;

public class BankAccount implements Serializable {
    private int dollars;
    private int euros;

    public BankAccount(int dollars, int euros) {
        this.dollars = dollars;
        this.euros = euros;
    }

    public int getDollars() {
        return dollars;
    }

    public void setDollars(int dollars) {
        this.dollars = dollars;
    }

    public int getEuros() {
        return euros;
    }

    public void setEuros(int euros) {
        this.euros = euros;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "dollars=" + dollars +
                ", euros=" + euros +
                '}';
    }
}
