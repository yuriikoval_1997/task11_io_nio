package classes_for_serialization;

import java.io.Serializable;

public class User implements Serializable {
    private static int counter;
    private int userId;
    private String userName;
    private String userEmail;
    private transient String password;
    private int age;
    private BankAccount bankAccount;

    public User(){
    }

    public User(String userName, String userEmail, String password, int age, BankAccount bankAccount) {
        this.userId = ++counter;
        this.userName = userName;
        this.userEmail = userEmail;
        this.password = password;
        this.age = age;
        this.bankAccount = bankAccount;
    }

    public static int getCounter() {
        return counter;
    }

    public static void setCounter(int counter) {
        User.counter = counter;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", userEmail='" + userEmail + '\'' +
                ", password='" + password + '\'' +
                ", age=" + age +
                ", bankAccount=" + bankAccount.toString() +
                '}';
    }
}
