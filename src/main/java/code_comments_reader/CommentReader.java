package code_comments_reader;

import java.io.*;
import java.util.*;


public class CommentReader {
    private static String LINE_SEPARATOR = System.lineSeparator();
    private static List<String> commentList = new ArrayList<>();
    private static StringBuilder builder = new StringBuilder();

    public static List<String> getAllComments(File javaCode) throws IOException {
        boolean isPartOfComment = false;
        int lineCounter = 1;
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(javaCode)))){
            while (reader.ready()){
                String line = reader.readLine();
                if (! isPartOfComment && line.contains("//")){
                    String commentPart = line.substring(line.indexOf("//")).concat(LINE_SEPARATOR);
                    commentList.add(commentPart);
                } else if (! isPartOfComment && line.contains("/*")){
                    String commentPart = line.substring(line.indexOf("/*"));
                    isPartOfComment = continueComment(commentPart);
                } else if (! isPartOfComment && line.contains("/**")){
                    String commentPart = line.substring(line.indexOf("/**"));
                    isPartOfComment = continueComment(commentPart);
                } else if (isPartOfComment){
                    isPartOfComment = continueComment(line);
                } else if (line.contains("*/")){
                    throw new IllegalCommentException("This java code has invalid comment in line: " + lineCounter);
                }
                lineCounter++;
            }
        }
        if (isPartOfComment){
            throw new IllegalCommentException("This java code has invalid comment in line: " + lineCounter);
        }
        return commentList;
    }

    private static boolean continueComment(String commentPart){
        if (commentPart.contains("*/")){
            String commentEnding = commentPart.substring(0, commentPart.indexOf("*/") + 2);
            builder.append(commentEnding).append(LINE_SEPARATOR);
            commentList.add(builder.toString());
            builder = new StringBuilder();
            return false;
        }
        builder.append(commentPart).append(LINE_SEPARATOR);
        return true;
    }
}
