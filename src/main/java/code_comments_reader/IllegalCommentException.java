package code_comments_reader;

public class IllegalCommentException extends RuntimeException {
    public IllegalCommentException(){
    }

    public IllegalCommentException(String message){
        super(message);
    }
}
