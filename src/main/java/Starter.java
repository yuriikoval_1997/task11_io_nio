import classes_for_serialization.BankAccount;
import classes_for_serialization.User;
import code_comments_reader.CommentReader;
import file_system_walker.DirectoryWalker;
import readers.FileReaderBenchmark;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Scanner;

public class Starter {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        String fileName = "src/main/resources/texts/The Iliad of Homer.txt";
        File file = new File(fileName);
        FileReaderBenchmark.readWithoutBuffer(file);
        FileReaderBenchmark.readWithBuffer(file);
        FileReaderBenchmark.readWithBufferedReader(file);
        Path temp = serializeUser();
        deserializeUser(temp);
        //TODO - read a path from the console.
        String fileRoot = "C:\\Users\\User\\Desktop\\Java EPAM";
        File directory = new File(fileRoot);
        System.out.println("++++ Walk through a directory ++++");
        Scanner scanner = new Scanner(System.in);
        String action;
        System.out.println("1 - walk using deque.");
        System.out.println("2 - walk using recursion.");
        System.out.println("3 - walk using Files.walk.");
        System.out.println("C - continue.");
        System.out.println("Q - quit");
        label:
        do {
            action = scanner.nextLine();
            switch (action.toLowerCase()) {
                case "1":
                    DirectoryWalker.walkUsingDeque(directory);
                    break label;
                case "2":
                    DirectoryWalker.walkUsingRecursion(directory);
                    break label;
                case "3":
                    DirectoryWalker.walkUsingStreamAPI(directory);
                    break label;
                case "c":
                    break label;
                case "q":
                    return;
                default:
                    System.out.println("Try again.");
                    break;
            }
        } while (true);
        List<String> commentList = CommentReader.getAllComments(
                new File("src/main/resources/java_code_sample/TreeOverlook.java"));
        System.out.println("++++ Here are all of the comments ++++");
        commentList.forEach(System.out::print);
        System.out.println("++++++++++++++++++++++++++++++++++++++");
    }

    private static Path serializeUser() throws IOException {
        BankAccount bankAccount = new BankAccount(100, 300);
        User user = new User("Yurii", "yurii.mail_sample@gmail.com", "password", 21, bankAccount);
        Path temp = Files.createTempFile("temp-", ".tmp");
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(temp.toFile()))) {
            objectOutputStream.writeObject(user);
        }
        return temp;
    }

    private static void deserializeUser(Path userPath) throws IOException, ClassNotFoundException {
        User user;
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(userPath.toFile()))){
            user = (User) objectInputStream.readObject();
        }
        System.out.println("---- This user-object has just been deserialized ----");
        System.out.println(user);
        System.out.println("-----------------------------------------------------");
    }
}
