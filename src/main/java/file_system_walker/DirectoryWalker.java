package file_system_walker;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Stream;

public class DirectoryWalker {
    public static void walkUsingDeque(File directory){
        isDirectory(directory);
        Deque<File> deque = new ArrayDeque<>();
        deque.addLast(directory);
        while (!deque.isEmpty()){
            File innerFile = deque.removeFirst();
            List<File> children = new ArrayList<>();
            for (File f : Objects.requireNonNull(innerFile.listFiles())){
                if (f.isDirectory()){
                    deque.addLast(f);
                }
                children.add(f);
            }
            printTree(innerFile, children);
        }
    }

    public static void walkUsingStreamAPI(File directory) throws IOException {
        try(Stream<Path> pathStream = Files.walk(directory.toPath())){
            pathStream.forEach(e -> {
                if (Files.isDirectory(e)){
                    File f = e.toFile();
                    printTree(f, Arrays.asList(Objects.requireNonNull(f.listFiles())));
                }
            });
        }
    }

    public static void walkUsingRecursion(File directory){
        isDirectory(directory);
        recursion(directory);
    }

    private static void recursion(File directory){
        for (File file : Objects.requireNonNull(directory.listFiles())){
            List<File> children = new ArrayList<>();
            if (file.isDirectory()){
                recursion(file);
            }
            children.add(file);
            printTree(directory, children);
        }
    }

    @Deprecated
    private static void printTree(File parent, List<File> children){
        System.out.print(parent.getName() + "{");
        children.forEach(e -> System.out.print(e.getName() + ", "));
        System.out.println("}");
    }

    private static void isDirectory(File directory){
        if (!directory.exists()){
            throw new IllegalArgumentException("Such a directory does not exist!");
        }
    }
}
