import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;

/*
Розрахувати висоту дерева
 */
class TreeOverlook {
    /**
     * Lorem ipsum dolor sit amet, scripta erroribus interpretaris ne his, sale numquam te usu. Ea cum wisi verterem.
     * Et primis sadipscing est, per at paulo noster tritani, et mei adhuc omnes viderer. Eu sit partem commodo mentitum,
     * ex vim diam alia posse, duo an docendi lobortis. Ius inani referrentur te, ut vim persius percipitur repudiandae,
     * choro graeci debitis in eum.
     * Erroribus assentior pri ad, in vim inimicus similique.
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        // For validator
//        BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));
//        int n = Integer.parseInt(consoleReader.readLine()); // == data.length
//        String[] temp = consoleReader.readLine().split(" ");
//        consoleReader.close();
//
//        int[] nodes = new int[temp.length];
//        for (int i = 0; i < temp.length; i++) {
//            nodes[i] = Integer.parseInt(temp[i]);
//        }
//
//        int depth = findDepthViaRecursion(nodes);
//        System.out.println(depth);


        // Test code
        ArrayList<String[]> data = new ArrayList<>();
        data.add("4 -1 4 1 1".split(" "));// depth is 3
        data.add("-1 0 4 0 3".split(" ")); // depth is 4
        data.add("9 7 5 5 2 9 9 9 2 -1".split(" ")); // depth is 4

        for (String[] numbers : data) {
            // INDEXES are nodes, VALUES are parents.
            int[] nodes = new int[numbers.length];
            for (int i = 0; i < numbers.length; i++) {
                nodes[i] = Integer.parseInt(numbers[i]);
            }
            System.out.println("Using recursion " + findDepthViaRecursion(nodes));
            System.out.println("Using stack " + findDepthViaStack(nodes));
            System.out.println("Using loop " + findDepthViaLoop(nodes));
        }
    }


    // INDEXES are nodes, VALUES are parents.
    private static int findDepthViaLoop(final int[] nodes){
        final int[] depthStorage = new int[nodes.length];
        int max = 0;
        for (int i = 0; i < nodes.length; i++) {
            int depth = usingLoop(nodes, i, depthStorage);
            if (max < depth){
                max = depth;
            }
        }
        return max;
    }

    private static int usingLoop(final int[] nodes, int nodeIndex, final int[] depthStorage){
        int localDepth = 0;
        int parent = nodes[nodeIndex];
        while (true){
            if (parent == -1){
                localDepth++;
                break;
            }
            if (depthStorage[nodeIndex] != 0){
                localDepth += depthStorage[nodeIndex];
                break;
            }
            nodeIndex = parent;
            parent = nodes[nodeIndex];
            localDepth++;
        }
        return localDepth;
    }


    // INDEXES are nodes, VALUES are parents.
    private static int findDepthViaStack(final int[] nodes){
        final int[] depthStorage = new int[nodes.length]; // indexes are nodes, values are depth
        int max = 0;
        for (int i = 0; i < nodes.length; i++) {
            int depth = usingStack(nodes, i, depthStorage);
            if (max < depth){
                max = depth;
            }
        }
        return max;
    }

    /** anjdbibibihadsbh biabhsfdbhr bhahhbh hdadbhbf*/
    private static int usingStack(final int[] nodes, int nodeIndex, final int[] depthStorage){
        Stack<Integer> stack = new Stack<>();
        int parent = nodes[nodeIndex];
        int localDepth = 0;
        stack.add(parent);
        while (! stack.isEmpty()){
            parent = stack.pop();
            if (parent == -1){
                localDepth++;
                break;
            }
            if (depthStorage[nodeIndex] != 0){
                localDepth += depthStorage[nodeIndex];
            }
            nodeIndex = parent;
            parent = nodes[nodeIndex];
            stack.add(parent);
            localDepth++;
        }
        return localDepth;
    }


    // INDEXES are nodes, VALUES are parents.
    private static int findDepthViaRecursion(final int[] nodes) {
        // Indexes are nodes, values are depth.
        int[] depthStorage = new int[nodes.length];
        int depth = 0;
        final int localDepth = 0;
        for (int i = 0; i < nodes.length; i++) {
            int max = usingRecursion(nodes, i, localDepth, depthStorage);
            depthStorage[i] = max;
            if (max > depth) {
                depth = max;
            }
        }
        return depth;
    }

    private static int usingRecursion(final int[] nodes, int nodeIndex, int localDepth, final int[] depthStorage) {
        if (nodes[nodeIndex] == -1) {
            return ++localDepth;
        }
        // if the usingRecursion has already visited this node and learnt the depth.
        if (depthStorage[nodeIndex] > 1) return depthStorage[nodeIndex] + localDepth;
        return usingRecursion(nodes, nodes[nodeIndex], ++localDepth, depthStorage);
    }
}